<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Repository\ContactRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Serializer;
use PhpParser\Node\Expr\BinaryOp\Concat;

/**
 * @Route("/api/contact", name="api_budget")
 */

class ContactController extends Controller
{
    private $serializer;

    public function __construct()
    {
        $encoder = new JsonEncoder();
        $normalizer = new GetSetMethodNormalizer();
        $normalizer->setCircularReferenceLimit(1);
        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });
        $this->serializer = new Serializer([$normalizer], [$encoder]);
    }

    /**
     * @Route("/{id}", name="one_contact", methods="GET")
     */
    public function findById(Contact $contact) : Response
    {
        $data = $this->serializer->normalize($contact, null, ['attributes' => ['id', 'name', 'surname','phone','role','address','picture', 'tags ' =>['id','name']]]);
        $response = new Response($this->serializer->serialize($data, "json"));
        return $response;
    }

    /**
     * @Route("", name="all_contacts", methods={"GET"})
     */
    public function all(ContactRepository $repo)
    {
        $list = $repo->findAll();

        $data = $this->serializer->normalize($list, null, ['attributes' => ['id', 'name', 'surname','phone','role','address','picture', 'tags ' =>['id','name']]]);

        $response = new Response($this->serializer->serialize($data, 'json'));
        return $response;
    }

    /**
     * @Route("", name="new_contact", methods={"POST"})
     */
    public function addContact(Request $request) : Response
    {
        $manager = $this->getDoctrine()->getManager();

        $content = $request->getContent();
        $contact = $this->serializer->deserialize($content, Contact::class, "json");

        $manager->persist($contact);
        $manager->flush();

        $data = $this->serializer->normalize($contact, null, ['attributes' => ['id', 'name', 'surname','phone','role','address','picture']]);

        $response = new Response($this->serializer->serialize($contact, "json"));
        return $response;
    }

    /**
     * @Route("/{id}", name="update_contact", methods={"PUT"})
     */
    public function update(Request $request, Contact $contact)
    {
        $manager = $this->getDoctrine()->getManager();

        $content = $request->getContent();
        $update = $this->serializer->deserialize($content, Contact::class, "json");

        $contact->setName($update->getName());
        $contact->setSurname($update->getSurname());
        $contact->setAddress($update->getAddress());
        $contact->setPhone($update->getPhone());
        $contact->setPicture($update->getPicture());
        $contact->setRole($update->getRole());

        $manager->persist($contact);
        $manager->flush();

        $data = $this->serializer->normalize($contact, null, ['attributes' => ['id', 'name', 'surname','phone','role','address','picture', 'tags ' =>['id','name']]]);

        $response = new Response($this->serializer->serialize($data, "json"));
        return $response;
    }

    /**
     * @Route("/{id}", name="delete_contact", methods={"DELETE"})
     */
    public function delete(Contact $contact)
    {
        $manager = $this->getDoctrine()->getManager();

        $manager->remove($contact);
        $manager->flush();

        return new Response("OK", 204);
    }
}
